### Estate Browser

This is a simple App that is able to display a list of Estates, operate on them with CRUD operations, filter them by date and prices.
It is developed using combined `.NET 6` and `ReactJS` for a simple and clean look.

In order to run the application:
- Install .NET SDK
- Install Node.js
- Run `dotnet run` in directory of `EstateBrowser.WebApi` project
- Run `npm start` in directory of `frontend/estate-browser`

After a while web browser window should pop up with app opened in it