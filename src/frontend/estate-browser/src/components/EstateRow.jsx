import { format } from "date-fns";
import { numberWithSpaces } from "../Utils";

export const DATE_FORMAT_SHORT_YEAR = "dd/MM/yy";

export const EstateRow = ({
    entry, updateFn, showModalFn
}) => {

  const deleteEstate = async () => {
    console.log('🙈', entry, `https://localhost:7243/api/estates/${entry.id}`)
    await fetch(`https://localhost:7243/api/estates/${entry.id}`, {
      method: 'DELETE',
  })
    updateFn()
  }

  return (
      <tr>
        <td>{entry.id}</td>
        <td>{entry.name}</td>
        <td>${numberWithSpaces(entry.price)}</td>
        <td>{format(new Date(entry.postDate), DATE_FORMAT_SHORT_YEAR)}</td>
        <td><button className="editButton" onClick={showModalFn}><span className='text'>Edit</span></button></td>
        <td><button className="deleteButton" onClick={deleteEstate}><span className='text'>Delete</span></button></td>
      </tr>
  )
};