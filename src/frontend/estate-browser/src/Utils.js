export const thousandsFormatter = (n) => {
    return n.toLocaleString("en-US").replaceAll(",", " "); 
  };
  
  export const largeNumberAbbreviation = (n) => {
    if (n >= 100_000) {
      return ("en-US", {
        notation: "compact",
        maximumFractionDigits: 1,
      }).format(n);
    }
  
    return thousandsFormatter(n);
  };

 export function numberWithSpaces(x) {
    var parts = x.toString().split(".");
    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, " ");
    return parts.join(".");
}