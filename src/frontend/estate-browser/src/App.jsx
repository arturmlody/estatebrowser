import Create from './Create.jsx';
import MainTable from './MainTable.jsx'

function App() {
  return (
    <div>
      <div className='content'>
        <MainTable/>
        <Create/>
      </div>
    </div>
  );
}

export default App;
