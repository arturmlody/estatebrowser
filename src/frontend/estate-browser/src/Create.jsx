import { useState, useEffect } from 'react';
import React from 'react';

function Create({estate}) {
    const callApi = async (
        name,
        price
    ) => {
        const response = await fetch(
            `https://localhost:7243/api/estates`,
            {
                method: estate ? 'PUT' : 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    id : estate?.id,
                    name,
                    price
                }),
            }
        );
    };
    console.log(estate)

    const [name, setName] = useState(estate?.name || "");
    const [price, setPrice] = useState(estate?.price || "");

    useEffect(() => {
        if(estate) {
            setName(estate.name);
            setPrice(estate.price);
        }
    }, [estate])


    return (
        <div>
            {!estate && <h1>Add new estate</h1>}
            <form
                onSubmit={() => {
                    callApi(
                        name,
                        price
                    )
                }}>
                <label>Name</label>
                <input
                    type="text"
                    value={name}
                    onChange={(e) => {
                        console.log(e.target.value)
                        setName(e.target.value)
                    }
                    }
                />

                <label>Price</label>
                <input
                    type="text"
                    value={price}
                    onChange={(e) =>
                        setPrice(e.target.value)
                    }
                />
                <div className='create'>
                    <button type="submit" className='addButton'>
                    {!estate ? 'Add' : 'Edit'}
                    </button>
                </div>
            </form>
        </div>
    );
}

export default Create;