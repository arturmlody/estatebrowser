import React, {useEffect, useState} from 'react';
import {EstateRow} from "./components/EstateRow.jsx";
import Create from './Create.jsx';
import './MainTable.css';

const MainTable = () => {
    const [contents, setContents] = useState([]);
    const [modalActive, setModalActive] = useState(false);
    const [estate, setEstate] = useState(undefined);

    const getEstates = async () => {
        setContents(await getMethod());
    }

    useEffect( () => {
        getEstates();
        }, []);

    const getMethod = async () => {
        const res = await fetch('https://localhost:7243/api/estates', {
            mode: 'cors'
        })
        const data = await res.json()
        return data
    };

    const openModal = (estate) => {
        setEstate(estate)
        setModalActive(true)
    }

    return (
        <div>
            <table className="styled-table">
                <tr>
                    <th>Id</th>
                    <th>Name</th>
                    <th>Price</th>
                    <th>Date posted</th>
                    <th>Edit</th>
                    <th>Delete</th>
                </tr>
                {contents.map((estate, index) => {
                    return (
                        <EstateRow
                            key={`${estate.name}-${index}`}
                            entry={estate}
                            updateFn={() => getEstates()}
                            showModalFn={() => openModal(estate)}
                        />
                    )
                })}
            </table>

            <div id="myModal" className={`modal ${modalActive ? "modal-active" : ""}`}>

            <div className="modal-content">
                <button className="close" onClick={() => setModalActive(false)}>🍌</button>
                <Create estate={estate}/>
            </div>

            </div>
        </div>
    );
}

export default MainTable;