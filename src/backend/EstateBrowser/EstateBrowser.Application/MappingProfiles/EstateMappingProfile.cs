﻿using AutoMapper;
using EstateBrowser.Application.Estates.Requests;
using EstateBrowser.Application.Estates.Responses;
using EstateBrowser.Domain.Entities;

namespace EstateBrowser.Application.MappingProfiles
{
    public class EstateMappingProfile : Profile
    {
        public EstateMappingProfile()
        {
            CreateMap<Estate, CreateEstateRequest>().ReverseMap();
            CreateMap<Estate, UpdateEstateRequest>().ReverseMap();
            CreateMap<Estate, EstateResponse>().ReverseMap();
        }
    }
}
