﻿using AutoMapper;
using EstateBrowser.Application.Estates.Interfaces;
using EstateBrowser.Application.Estates.Requests;
using EstateBrowser.Application.Estates.Responses;
using EstateBrowser.Application.Interfaces;
using EstateBrowser.Domain.Entities;

namespace EstateBrowser.Application.Estates.Handlers
{
    public class UpdateEstateRequestHandler : IUpdateEstateRequestHandler
    {
        private readonly IEstateRepository _estateRepository;
        private readonly IMapper _mapper;

        public UpdateEstateRequestHandler(IMapper mapper, IEstateRepository estateRepository)
        {
            _mapper = mapper;
            _estateRepository = estateRepository;
        }

        public async Task<EstateResponse> Handle(UpdateEstateRequest request, CancellationToken token = default)
        {
            var estate = _estateRepository.Estates.FirstOrDefault(x => x.Id == request.Id);
            if (estate == null)
            {
                return new EstateResponse();
            }

            var mappedEstate = _mapper.Map<Estate>(request);
            var updatedEstate = await _estateRepository.UpdateEstateAsync(mappedEstate);

            var response = _mapper.Map<EstateResponse>(updatedEstate);

            return response;
        }
    }
}
