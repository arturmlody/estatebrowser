﻿using AutoMapper;
using EstateBrowser.Application.Estates.Interfaces;
using EstateBrowser.Application.Estates.Responses;
using EstateBrowser.Application.Interfaces;

namespace EstateBrowser.Application.Estates.Handlers
{
    public class GetAllEstatesHandler : IGetAllEstatesHandler
    {
        private readonly IEstateRepository _estateRepository;
        private readonly IMapper _mapper;

        public GetAllEstatesHandler(IEstateRepository estateRepository, IMapper mapper)
        {
            _estateRepository = estateRepository;
            _mapper = mapper;
        }

        public Task<IEnumerable<EstateResponse>> Handle(CancellationToken token = default)
        {
            var entities = _estateRepository.Estates.ToList();
            var mapped = _mapper.Map<IEnumerable<EstateResponse>>(entities);

            return Task.FromResult(mapped);
        }
    }
}
