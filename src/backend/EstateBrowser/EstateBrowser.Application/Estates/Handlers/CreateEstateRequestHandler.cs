﻿using AutoMapper;
using EstateBrowser.Application.Estates.Interfaces;
using EstateBrowser.Application.Estates.Requests;
using EstateBrowser.Application.Estates.Responses;
using EstateBrowser.Application.Interfaces;
using EstateBrowser.Domain.Entities;

namespace EstateBrowser.Application.Estates.Handlers
{
    public class CreateEstateRequestHandler : ICreateEstateRequestHandler
    {
        private readonly IEstateRepository _estateRepository;
        private readonly IMapper _mapper;

        public CreateEstateRequestHandler(IEstateRepository estateRepository, IMapper mapper)
        {
            _estateRepository = estateRepository;
            _mapper = mapper;
        }

        public async Task<EstateResponse> Handle(CreateEstateRequest request, CancellationToken token = default)
        {
            var estate = _mapper.Map<Estate>(request);
            estate.PostDate = DateTimeOffset.UtcNow;

            var createdEstate = await _estateRepository.CreateEstateAsync(estate, token);

            var response = _mapper.Map<EstateResponse>(createdEstate);

            return response;
        }
    }
}
