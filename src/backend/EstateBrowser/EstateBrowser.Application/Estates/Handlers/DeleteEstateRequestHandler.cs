﻿using EstateBrowser.Application.Estates.Interfaces;
using EstateBrowser.Application.Estates.Requests;
using EstateBrowser.Application.Interfaces;

namespace EstateBrowser.Application.Estates.Handlers
{
    public class DeleteEstateRequestHandler : IDeleteEstateRequestHandler
    {
        private readonly IEstateRepository _estateRepository;

        public DeleteEstateRequestHandler(IEstateRepository estateRepository)
        {
            _estateRepository = estateRepository;
        }

        public async Task<int> Handle(DeleteEstateRequest request, CancellationToken token = default)
        {
            var entity = _estateRepository.Estates.FirstOrDefault(x => x.Id == request.Id);
            if (entity is null)
            {
                return 0;
            }

            var response = await _estateRepository.DeleteEstateAsync(entity, token);
            return response;
        }
    }
}
