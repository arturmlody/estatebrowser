﻿using AutoMapper;
using EstateBrowser.Application.Estates.Interfaces;
using EstateBrowser.Application.Estates.Responses;
using EstateBrowser.Application.Interfaces;

namespace EstateBrowser.Application.Estates.Handlers
{
    public class GetPriceFilteredEstatesHandler : IGetPriceFilteredEstatesHandler
    {
        private readonly IEstateRepository _estateRepository;
        private readonly IMapper _mapper;

        public GetPriceFilteredEstatesHandler(IMapper mapper, IEstateRepository estateRepository)
        {
            _mapper = mapper;
            _estateRepository = estateRepository;
        }

        public Task<IEnumerable<EstateResponse>> Handle(decimal min, decimal max, CancellationToken token = default)
        {
            var entities = _estateRepository.Estates
                .Where(x => x.Price >= min && x.Price <= max)
                .ToList();
            var mapped = _mapper.Map<IEnumerable<EstateResponse>>(entities);

            return Task.FromResult(mapped);
        }
    }
}
