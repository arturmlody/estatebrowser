﻿using AutoMapper;
using EstateBrowser.Application.Estates.Interfaces;
using EstateBrowser.Application.Estates.Responses;
using EstateBrowser.Application.Interfaces;

namespace EstateBrowser.Application.Estates.Handlers
{
    public class GetDateFilteredEstatesHandler : IGetDateFilteredEstatesHandler
    {
        private readonly IEstateRepository _estateRepository;
        private readonly IMapper _mapper;

        public GetDateFilteredEstatesHandler(IMapper mapper, IEstateRepository estateRepository)
        {
            _mapper = mapper;
            _estateRepository = estateRepository;
        }

        public Task<IEnumerable<EstateResponse>> Handle(DateTimeOffset startDate, DateTimeOffset endDate, CancellationToken token = default)
        {
            var entities = _estateRepository.Estates
                .Where(x => x.PostDate >= startDate && x.PostDate <= endDate)
                .ToList();
            var mapped = _mapper.Map<IEnumerable<EstateResponse>>(entities);

            return Task.FromResult(mapped);
        }
    }
}
