﻿namespace EstateBrowser.Application.Estates.Requests
{
    public record DeleteEstateRequest(int Id)
    {
    }
}
