﻿namespace EstateBrowser.Application.Estates.Requests
{
    public class CreateEstateRequest
    {
        public string? Name { get; set; }
        public decimal Price { get; set; }
    }
}
