﻿namespace EstateBrowser.Application.Estates.Requests
{
    public class UpdateEstateRequest
    {
        public int Id { get; set; }
        public string? Name { get; set; }
        public decimal Price { get; set; }
    }
}
