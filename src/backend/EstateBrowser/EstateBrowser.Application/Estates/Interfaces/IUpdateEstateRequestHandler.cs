﻿using EstateBrowser.Application.Estates.Requests;
using EstateBrowser.Application.Estates.Responses;

namespace EstateBrowser.Application.Estates.Interfaces
{
    public interface IUpdateEstateRequestHandler
    {
        Task<EstateResponse> Handle(UpdateEstateRequest request, CancellationToken token = default);
    }
}