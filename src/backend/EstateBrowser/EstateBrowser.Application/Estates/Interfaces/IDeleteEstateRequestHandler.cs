﻿using EstateBrowser.Application.Estates.Requests;

namespace EstateBrowser.Application.Estates.Interfaces
{
    public interface IDeleteEstateRequestHandler
    {
        Task<int> Handle(DeleteEstateRequest request, CancellationToken token = default);
    }
}