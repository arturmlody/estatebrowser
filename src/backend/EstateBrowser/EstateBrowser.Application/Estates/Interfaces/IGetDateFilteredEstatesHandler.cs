﻿using EstateBrowser.Application.Estates.Responses;

namespace EstateBrowser.Application.Estates.Interfaces
{
    public interface IGetDateFilteredEstatesHandler
    {
        Task<IEnumerable<EstateResponse>> Handle(DateTimeOffset startDate, DateTimeOffset endDate, CancellationToken token = default);
    }
}
