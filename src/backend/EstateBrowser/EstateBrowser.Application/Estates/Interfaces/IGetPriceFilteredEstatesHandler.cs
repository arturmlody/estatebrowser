﻿using EstateBrowser.Application.Estates.Responses;

namespace EstateBrowser.Application.Estates.Interfaces
{
    public interface IGetPriceFilteredEstatesHandler
    {
        Task<IEnumerable<EstateResponse>> Handle(decimal min, decimal max, CancellationToken token = default);
    }
}
