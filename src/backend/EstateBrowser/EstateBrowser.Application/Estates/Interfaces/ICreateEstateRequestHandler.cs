﻿using EstateBrowser.Application.Estates.Requests;
using EstateBrowser.Application.Estates.Responses;

namespace EstateBrowser.Application.Estates.Interfaces
{
    public interface ICreateEstateRequestHandler
    {
        Task<EstateResponse> Handle(CreateEstateRequest request, CancellationToken token = default);
    }
}