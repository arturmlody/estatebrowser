﻿using EstateBrowser.Application.Estates.Responses;

namespace EstateBrowser.Application.Estates.Interfaces
{
    public interface IGetAllEstatesHandler
    {
        Task<IEnumerable<EstateResponse>> Handle(CancellationToken token = default);
    }
}