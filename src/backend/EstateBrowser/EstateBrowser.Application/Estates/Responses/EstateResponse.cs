﻿namespace EstateBrowser.Application.Estates.Responses
{
    public class EstateResponse
    {
        public int Id { get; set; }
        public string Name { get; set; } = default!;
        public DateTimeOffset PostDate { get; set; }
        public decimal Price { get; set; }
    }
}
