﻿using EstateBrowser.Domain.Entities;

namespace EstateBrowser.Application.Interfaces
{
    public interface IEstateRepository
    {
        IQueryable<Estate> Estates { get; }

        Task<Estate> CreateEstateAsync(Estate estate, CancellationToken token = default);
        Task<Estate> UpdateEstateAsync(Estate estate, CancellationToken token = default);
        Task<int> DeleteEstateAsync(Estate estate, CancellationToken token = default);
    }
}
