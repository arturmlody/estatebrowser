﻿using EstateBrowser.Application.Estates.Handlers;
using EstateBrowser.Application.Estates.Interfaces;
using FluentValidation;
using Microsoft.Extensions.DependencyInjection;

namespace EstateBrowser.Application.Extensions
{
    public static class ConfigureServicesExtension
    {
        public static void AddApplicationServices(this IServiceCollection services)
        {
            services.AddScoped<ICreateEstateRequestHandler, CreateEstateRequestHandler>();
            services.AddScoped<IDeleteEstateRequestHandler, DeleteEstateRequestHandler>();
            services.AddScoped<IGetAllEstatesHandler, GetAllEstatesHandler>();
            services.AddScoped<IUpdateEstateRequestHandler, UpdateEstateRequestHandler>();
            services.AddScoped<IGetDateFilteredEstatesHandler, GetDateFilteredEstatesHandler>();
            services.AddScoped<IGetPriceFilteredEstatesHandler, GetPriceFilteredEstatesHandler>();
            services.AddAutoMapper(typeof(ConfigureServicesExtension).Assembly);
            services.AddValidatorsFromAssembly(typeof(ConfigureServicesExtension).Assembly);
        }
    }
}
