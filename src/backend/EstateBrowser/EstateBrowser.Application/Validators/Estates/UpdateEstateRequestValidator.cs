﻿using EstateBrowser.Application.Estates.Requests;
using FluentValidation;

namespace EstateBrowser.Application.Validators.Estates
{
    public class UpdateEstateRequestValidator : AbstractValidator<UpdateEstateRequest>
    {
        public UpdateEstateRequestValidator()
        {
            RuleFor(x => x.Name)
                .NotEmpty()
                .WithMessage("Name should not be empty!")
                .MaximumLength(100)
                .WithMessage("Name should be under 100 characters!");
            RuleFor(x => x.Price)
                .NotEmpty()
                .WithMessage("Estate should have a price!");
        }
    }
}
