﻿namespace EstateBrowser.Domain.Entities
{
    public class Estate
    {
        public int Id { get; set; }
        public string Name { get; set; } = default!;
        public DateTimeOffset PostDate { get; set; }
        public decimal Price { get; set; }
    }
}
