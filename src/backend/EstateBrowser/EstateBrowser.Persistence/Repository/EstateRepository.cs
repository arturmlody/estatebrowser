﻿using EstateBrowser.Application.Interfaces;
using EstateBrowser.Domain.Entities;

namespace EstateBrowser.Persistence.Repository
{
    public class EstateRepository : IEstateRepository
    {
        private InMemoryContext _context;

        public EstateRepository(InMemoryContext context)
        {
            _context = context;
            if (_context.Estates.Any() == false)
            {
                SeedData();
            }
        }

        public IQueryable<Estate> Estates => _context.Estates;

        public async Task<Estate> CreateEstateAsync(Estate estate, CancellationToken token = default)
        {
            _context.Add(estate);
            await _context.SaveChangesAsync(token);
            return estate;
        }

        public async Task<int> DeleteEstateAsync(Estate estate, CancellationToken token = default)
        {
            _context.Remove(estate);
            await _context.SaveChangesAsync(token);
            return estate.Id;
        }

        public async Task<Estate> UpdateEstateAsync(Estate estate, CancellationToken token = default)
        {
            var estateToChange = _context.Estates.First(x => x.Id == estate.Id);
            estateToChange.Name = estate.Name;
            estateToChange.Price = estate.Price;
            await _context.SaveChangesAsync(token);
            return estate;
        }

        private void SeedData()
        {
            _context.AddRange(
                new Estate
                {
                    Id = 1,
                    Name = "Beverly Hills",
                    Price = 250000000m,
                    PostDate = DateTimeOffset.Now.AddDays(-20)
                },
                new Estate
                {
                    Id = 2,
                    Name = "Warsaw Skytower",
                    Price = 370000000m,
                    PostDate = DateTimeOffset.Now.AddDays(-10)
                },
                new Estate
                {
                    Id = 3,
                    Name = "White house",
                    Price = 50m,
                    PostDate = DateTimeOffset.Now.AddDays(-1)
                },
                new Estate
                {
                    Id = 4,
                    Name = "Nakatomi Tower",
                    Price = 120000000m,
                    PostDate = DateTimeOffset.Now.AddDays(5)
                },
                new Estate
                {
                    Id = 5,
                    Name = "Blue Lagoon",
                    Price = 70000000m,
                    PostDate = DateTimeOffset.Now.AddDays(-57)
                },
                new Estate
                {
                    Id = 6,
                    Name = "Tokyo Dome",
                    Price = 6000000000m,
                    PostDate = DateTimeOffset.Now.AddDays(-123)
                },
                new Estate
                {
                    Id = 7,
                    Name = "Burj Khalifa",
                    Price = 150000000m,
                    PostDate = DateTimeOffset.Now.AddSeconds(-10)
                });
            _context.SaveChanges();
        }
    }
}
