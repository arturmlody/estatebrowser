﻿using EstateBrowser.Application.Interfaces;
using EstateBrowser.Persistence.Repository;
using Microsoft.Extensions.DependencyInjection;

namespace EstateBrowser.Persistence.Extensions
{
    public static class ConfigureServicesExtension
    {
        public static void AddPersistenceServices(this IServiceCollection services)
        {
            services.AddDbContext<InMemoryContext>();
            services.AddScoped<IEstateRepository, EstateRepository>();
        }
    }
}
