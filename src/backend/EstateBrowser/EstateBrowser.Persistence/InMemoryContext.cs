﻿using EstateBrowser.Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace EstateBrowser.Persistence
{
    public class InMemoryContext : DbContext
    {
        public InMemoryContext(DbContextOptions<InMemoryContext> options) : base(options)
        {

        }

        public DbSet<Estate> Estates { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseInMemoryDatabase(databaseName: "EstateDb");
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            var estate = builder.Entity<Estate>();
            estate.HasKey(x => x.Id);
            estate.Property(x => x.Name).IsRequired();
            estate.Property(x => x.Price).IsRequired();
        }
    }
}
