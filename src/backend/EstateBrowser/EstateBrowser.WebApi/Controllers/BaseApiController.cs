﻿using FluentValidation.Results;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace EstateBrowser.WebApi.Controllers
{
    public abstract class BaseApiController : ControllerBase
    {
        protected IActionResult GetValidationErrors(ValidationResult validationResult)
        {
            var modelStateDictionary = new ModelStateDictionary();
            foreach (var error in validationResult.Errors)
            {
                modelStateDictionary.AddModelError(error.PropertyName, error.ErrorMessage);
            }
            return ValidationProblem(modelStateDictionary);
        }
    }
}
