﻿using EstateBrowser.Application.Estates.Interfaces;
using EstateBrowser.Application.Estates.Requests;
using FluentValidation;
using Microsoft.AspNetCore.Mvc;

namespace EstateBrowser.WebApi.Controllers
{
    [Route("api/[controller]")]
    public class EstatesController : BaseApiController
    {
        private readonly IGetAllEstatesHandler _getAllEstatesHandler;
        private readonly ICreateEstateRequestHandler _createEstateRequestHandler;
        private readonly IUpdateEstateRequestHandler _updateEstateRequestHandler;
        private readonly IDeleteEstateRequestHandler _deleteEstateRequestHandler;
        private readonly IGetPriceFilteredEstatesHandler _getPriceFilteredEstatesHandler;
        private readonly IGetDateFilteredEstatesHandler _getDateFilteredEstatesHandler;
        private readonly IValidator<CreateEstateRequest> _createEstateRequestValidator;
        private readonly IValidator<UpdateEstateRequest> _updateEstateRequestValidator;

        public EstatesController(IGetAllEstatesHandler getAllEstatesHandler,
            ICreateEstateRequestHandler createEstateRequestHandler,
            IUpdateEstateRequestHandler updateEstateRequestHandler,
            IDeleteEstateRequestHandler deleteEstateRequestHandler,
            IValidator<CreateEstateRequest> createEstateRequestValidator,
            IValidator<UpdateEstateRequest> updateEstateRequestValidator,
            IGetPriceFilteredEstatesHandler getPriceFilteredEstatesHandler,
            IGetDateFilteredEstatesHandler getDateFilteredEstatesHandler)
        {
            _getAllEstatesHandler = getAllEstatesHandler;
            _createEstateRequestHandler = createEstateRequestHandler;
            _updateEstateRequestHandler = updateEstateRequestHandler;
            _deleteEstateRequestHandler = deleteEstateRequestHandler;
            _createEstateRequestValidator = createEstateRequestValidator;
            _updateEstateRequestValidator = updateEstateRequestValidator;
            _getPriceFilteredEstatesHandler = getPriceFilteredEstatesHandler;
            _getDateFilteredEstatesHandler = getDateFilteredEstatesHandler;
        }

        [HttpGet]
        public async Task<IActionResult> GetAllEstates(CancellationToken cancellationToken = default)
        {
            var response = await _getAllEstatesHandler.Handle(cancellationToken);
            return Ok(response);
        }

        [HttpGet("pricesearch")]
        public async Task<IActionResult> GetEstatesFilteredByPrice([FromQuery] decimal min, [FromQuery] decimal max, CancellationToken cancellationToken = default)
        {
            var response = await _getPriceFilteredEstatesHandler.Handle(min, max, cancellationToken);
            return Ok(response);
        }

        [HttpGet("datasearch")]
        public async Task<IActionResult> GetEstatesFilteredByPostDate([FromQuery] DateTimeOffset startDate, [FromQuery] DateTimeOffset endDate, CancellationToken cancellationToken = default)
        {
            var response = await _getDateFilteredEstatesHandler.Handle(startDate, endDate, cancellationToken);
            return Ok(response);
        }

        [HttpPost]
        public async Task<IActionResult> CreateEstate([FromBody] CreateEstateRequest request, CancellationToken cancellationToken = default)
        {
            var validationResult = await _createEstateRequestValidator.ValidateAsync(request, cancellationToken);
            if (validationResult.IsValid)
            {
                var response = await _createEstateRequestHandler.Handle(request, cancellationToken);
                return Ok(response);
            }
            else
            {
                return GetValidationErrors(validationResult);
            }
        }

        [HttpPut]
        public async Task<IActionResult> UpdateEstate([FromBody] UpdateEstateRequest request, CancellationToken cancellationToken = default)
        {
            var validationResult = await _updateEstateRequestValidator.ValidateAsync(request, cancellationToken);
            if (validationResult.IsValid)
            {
                var response = await _updateEstateRequestHandler.Handle(request, cancellationToken);
                return Ok(response);
            }
            else
            {
                return GetValidationErrors(validationResult);
            }
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteEstate([FromRoute] int id, CancellationToken cancellationToken = default)
        {
            var request = new DeleteEstateRequest(id);
            var response = await _deleteEstateRequestHandler.Handle(request, cancellationToken);
            return Ok(response);
        }
    }
}
