using EstateBrowser.Application.Estates.Handlers;
using EstateBrowser.Application.MappingProfiles;
using EstateBrowser.Persistence;
using EstateBrowser.Persistence.Repository;
using Microsoft.EntityFrameworkCore;

namespace EstateBrowser.Application.UnitTests
{
    public class FilteringTests
    {
        private readonly IMapper _mapper;

        // Data is provided by SeedData method in repository. Whenever repository constructor is called and there is no data
        // it will try to seed. So records returned are seeded from that place

        public FilteringTests()
        {
            if (_mapper == null)
            {
                var mappingConfig = new MapperConfiguration(mc =>
                {
                    mc.AddProfile(new EstateMappingProfile());
                });
                IMapper mapper = mappingConfig.CreateMapper();
                _mapper = mapper;
            }
        }

        [Fact]
        public async void GetPriceFiltered_Given0to100million_Returns2Records()
        {
            //Arrange
            var options = new DbContextOptionsBuilder<InMemoryContext>()
                .UseInMemoryDatabase(databaseName: "testDatabase")
                .Options;

            using (var context = new InMemoryContext(options))
            {
                var repository = new EstateRepository(context);
                var sut = new GetPriceFilteredEstatesHandler(_mapper, repository);

                //Act
                var response = await sut.Handle(0, 100000000);

                //Assert
                response.Count().Should().Be(2);
            }
        }

        [Fact]
        public async void GetDateFiltered_GivenOneMonthPeriod_Returns4Records()
        {
            //Arrange
            var options = new DbContextOptionsBuilder<InMemoryContext>()
                .UseInMemoryDatabase(databaseName: "testDatabase")
                .Options;

            using (var context = new InMemoryContext(options))
            {
                var repository = new EstateRepository(context);
                var sut = new GetDateFilteredEstatesHandler(_mapper, repository);

                //Act
                var response = await sut.Handle(DateTimeOffset.Now.AddDays(-30), DateTimeOffset.Now);

                //Assert
                response.Count().Should().Be(4);
            }
        }
    }
}